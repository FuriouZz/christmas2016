'use strict'

const Renderer    = require('./renderer')
const AssetsStore = require('libs/assets-store').default
const MANIFEST    = require('./manifest')
const Scene       = require('./scene')

window.addEventListener('DOMContentLoaded', load)

function load() {

  if ( !MANIFEST.length ) init()

  AssetsStore.onLoadProgress = function(p){
    console.log( 'load progress - ', p)
  }

  AssetsStore.onLoadComplete = function(){
    console.log( 'load complete - init' )
    init()
  }

  AssetsStore.loadManifest( MANIFEST )

}

function init() {

  const renderer = new Renderer( document.getElementById('canvas') )
  renderer.play()
  renderer.updateSize()

  const scene = new Scene( renderer )

}
