'use strict'

const EventEmitter = require('events').EventEmitter

EventEmitter.prototype.off = EventEmitter.prototype.removeListener

const NotificationCenter = new EventEmitter
module.exports = NotificationCenter