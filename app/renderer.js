'use strict'

const NotificationCenter = require('lib/notification-center')
const Renderer = require('nanogl-renderer')
const Node     = require('nanogl-node')
const Camera   = require('nanogl-camera')

module.exports = Renderer({

  hdpi: false,
  // pixelRatio: 1,

  init() {

    this.camera = Camera.makePerspectiveCamera()
    this.camera.lens.setAutoFov( 43.68 / 180 * Math.PI )
    this.camera.lens.aspect = this.width / this.height
    this.camera.lens.far    = 1000.

    this.root = new Node
    this.root.add( this.camera )

    window.addEventListener('resize', this.resize)
  },

  getContextOptions() {
    return {
      depth: true,
      stencil: false,
      antialias: false
    }
  },

  render( dt ) {
    this.root.updateWorldMatrix()
    this.camera.updateViewProjectionMatrix( this.width, this.height )

    this.gl.clearColor( 0.0, 0.0, 0.0, 1.0 )
    this.gl.clear( this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT )
    this.gl.viewport(0, 0, this.width, this.height)

    NotificationCenter.emit('render', dt, this)
  },

  resize() {
    this.canvas.width        = window.innerWidth
    this.canvas.height       = window.innerHeight
    this.canvas.style.width  = `${this.canvas.width}px`
    this.canvas.style.height = `${this.canvas.height}px`

    NotificationCenter.emit('resize', window.innerWidth, window.innerHeight)
  }

})