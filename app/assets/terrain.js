'use strict'

const Node            = require('nanogl-node')
const AssetsStore     = require('libs/assets-store').default
const PlaneGeometry   = require('geometries/plane-geometry').default
const Geometry        = require('geometries/geometry').default
const BasicMaterial   = require('materials/basic-material').default
const NC              = require('../lib/notification-center')
const TerrainGeometry = require('./terrain-geometry')

module.exports = class Terrain extends Node {

  constructor( gl ) {
    super()

    this.render = this.render.bind(this)

    this.gl = gl

    NC.on('render', this.render)

    this.createGeometry()
  }

  getImageData( size, imageSize ) {
    const $canvas = document.createElement('canvas')
    const ctx     = $canvas.getContext('2d')
    const $img    = AssetsStore.get('assets/images/heightmap.png')

    $canvas.width  = size
    $canvas.height = size
    $canvas.style.width    = `${$canvas.width}px`
    $canvas.style.height   = `${$canvas.height}px`
    $canvas.style.position = `absolute`

    ctx.drawImage($img, 0, 0, imageSize, imageSize, 0, 0, size, size)

    // document.body.appendChild( $canvas )

    const imageData = ctx.getImageData(0, 0, size, size)
    this.imageData  = []

    for (let i = 0, len = imageData.data.length; i < len; i+=4) {
      this.imageData.push( imageData.data[i+0] / 255 )
    }
  }

  createGeometry() {
    const size = 128
    this.getImageData( size, 2048 )

    this.material = new BasicMaterial( this.gl )

    this.geometry = new PlaneGeometry( this.gl, {
      width: 10,
      height: 10,
      widthSegments: 250,
      heightSegments: 250
    })
    // this.geometry.drawingMethod = 'drawLineStrip'

    const max = this.geometry.vertices.length / 3

    for (let offset = 0, i = 2; i < this.geometry.vertices.length; i += 3, offset++) {

      const px = (this.geometry.vertices[i-2] + 5) / 10
      const py = 1 - (this.geometry.vertices[i-1] + 5) / 10

      this.geometry.vertices[i] = this.imageData[Math.floor(px * size) + (Math.floor(py * size) * size)]

    }

    this.geometry.buffer.data( this.geometry.vertices )

    this.setScale(100)

    // var width_half = 10 / 2;
    // var height_half = 10 / 2;

    // var gridX = Math.floor( 200 ) || 1;
    // var gridY = Math.floor( 200 ) || 1;

    // var gridX1 = gridX + 1;
    // var gridY1 = gridY + 1;

    // var segment_width = 10 / gridX;
    // var segment_height = 10 / gridY;

    // var offset = 0;
    // var offset2 = 0;

    // for ( var iy = 0; iy < gridY1; iy ++ ) {

    //   var y = iy * segment_height - height_half;

    //   for ( var ix = 0; ix < gridX1; ix ++ ) {

    //     var x = ix * segment_width - width_half;

    //     this.imageData[]

    //     this.geometry.vertices[ offset + 2 ] = 0

    //     offset += 3;
    //     offset2 += 2;

    //   }

    // }

    // const PrimitivePlane = require('primitive-plane')
    // const plane = PrimitivePlane(1, 1, 10, 10)

    // for (let i = 0, len = plane.cells.length, progress = 0; i < len; i++) {

    //   for (let j = 0; j < plane.cells[i].length; j++) {
    //     progress = Math.floor((plane.cells[i][j] / len) * this.imageData.length)
    //     // plane.positions[plane.cells[i][j]][2] = this.imageData[progress]
    //   }

    // }

    // const positions = [].concat.apply([], plane.positions)
    // const cells     = [].concat.apply([], plane.cells)

    // // this.geometry.buffer.data( this.geometry.vertices )

    // this.geometry = new Geometry( this.gl, positions, cells )
    // this.geometry.attrib( 'aPosition', 3, this.gl.FLOAT )

    // const heights = []
    // for (let i = 0; i < this.imageData.data.length; i += 4) {
    //   heights.push( this.imageData.data[i] / 255 )
    // }

    // console.log(heights)

  }

  render( dt, renderer ) {

    this.material.prepare( this, renderer.camera )

    this.geometry.bind( this.material.prg )
    this.geometry.render( renderer.camera )

  }

}