'use strict'

const Geometry = require('geometries/geometry').default

const _generate = function( imageData ) {

  let vertices  = []
  const indices = []

  const sizeX    = 10
  const sizeY    = 10

  const segmentsX = 5
  const segmentsY = 5

  for (let y = 0; y < segmentsY; y++) {
    for (let x = 0; x < segmentsX; x++) {

      vertices.push([
        (x / segmentsX) * sizeX,
        (y / segmentsY) * sizeY,
        0
      ])

    }
  }

  let offset = 0

  for (let y = 0; y < segmentsY; y++) {
    for (let x = 0; x < segmentsX; x++) {

      const a = x + segmentsX * y
      const b = x + segmentsX * (y + 1)
      const c = (x + 1) + segmentsX * y
      const d = (x + 1) + segmentsX * (y + 1)

      indices[ offset   ] = a
      indices[ offset+1 ] = c
      indices[ offset+2 ] = d

      indices[ offset+3 ] = a
      indices[ offset+4 ] = b
      indices[ offset+5 ] = d

      offset += 6

    }
  }


  vertices = [].concat.apply([], vertices)
  console.log( vertices, indices )

  // const vertices = []

  // const size = 5
  // const count = 200

  // for (let i = 0; i < count; i++) {

  //   const position = i % size

  //   const progress = Math.floor((i / count) * imageData.length)

  //   vertices.push( position )
  //   vertices.push( Math.floor(i / size) )
  //   vertices.push( 0 )

  // }

  // const indices  = []

  // const triangle = 3
  // const face     = 1//triangle * 2
  // const line     = (size - 1) * face
  // const lines    = line * ( size - 2 )



  // /**
  //  *
  //  *  0  1  2  3  4
  //  *  5  6  7  8  9
  //  * 10 11 12 13 14
  //  * 15 16 17 18 19
  //  *
  //  * 0-5-6, 0-1-6, 1-6-7, 1-2-7, 2-7-8, 2-3-8, 3-8-9, 3-4-9
  //  *
  //  * 5-10-11, 5-6-11, 6-11-12, 6-7-12, 7-12-13, 7-8-13, 8-13-14, 8-9-14
  //  *
  //  * 10-15-16, 10-11-16, 11-16-17, 11-12-17, 12-17-18, 12-13-18, 13-18-19, 13-14-19
  //  *
  //  *
  //  * 12
  //  *
  //  *  0  1  2  3  4
  //  *  5  6  7  8  9
  //  * 10 11 12 13 14
  //  * 15 16 17 18 19
  //  *
  //  *
  //  *
  //  */

  // const countIndice = (size - 1) * ((count/size)-1)

  // for (let i = 0; i < countIndice; i++) {

  //   const m = Math.floor(i / 4)

  //   indices.push( m + 0 + i )
  //   indices.push( m + size + 0 + i )
  //   indices.push( m + size + 1 + i )

  //   indices.push( m + 0 + i )
  //   indices.push( m + 1 + i )
  //   indices.push( m + size + 1 + i )

  // }

  return { vertices, indices }

}

module.exports = class TerrainGeometry extends Geometry {

  constructor( gl, imageData ) {
    const { vertices, indices } = _generate( imageData )
    super( gl, vertices, indices )

  }

  allocate() {
    super.allocate()
    this.buffer.attrib( 'aPosition', 3, this.gl.FLOAT )
  }

}