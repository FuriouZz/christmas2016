'use strict'

const Terrain     = require('./assets/terrain')
const Node        = require('nanogl-node')
const MaxControls = require('./common/camera/max-controls')
const BlenderControls = require('./common/camera/blender-controls')
const FlowerControls = require('./common/camera/flower-controls')
const OrbitControls = require('./common/camera/orbit-controls').default
const NC          = require('./lib/notification-center')

const Cube = require('entities/cube').default
const BasicMaterial = require('materials/basic-material').default

class Scene extends Node {

  constructor( renderer ) {
    super()

    this.render = this.render.bind(this)


    this.renderer = renderer
    this.renderer.root.add( this )

    this.gl = renderer.gl

    const terrain = new Terrain( this.gl )
    terrain.rotateX(Math.PI * -0.5)
    this.add( terrain )

    this.setupControls()


    this.cube0 = new Cube( new BasicMaterial(this.gl, { diffuseColor: [1, 0, 0] }) )
    this.add( this.cube0 )

    this.cube1 = new Cube( new BasicMaterial(this.gl, { diffuseColor: [0, 1, 0] }) )
    this.cube1.y = 2
    this.cube1.z = 3
    this.add( this.cube1 )

    this.cube2 = new Cube( new BasicMaterial(this.gl, { diffuseColor: [0, 0, 1] }) )
    this.cube2.x = 0
    this.cube2.z = -7
    this.add( this.cube2 )

    this.cube3 = new Cube( new BasicMaterial(this.gl, { diffuseColor: [0, 1, 1] }) )
    this.cube3.x = -2
    this.cube3.y = 3
    this.cube3.z = -7
    this.add( this.cube3 )

    this.cube4 = new Cube( new BasicMaterial(this.gl, { diffuseColor: [1, 1, 0] }) )
    this.cube4.x = 2
    this.cube4.y = -3
    this.cube4.z = 1
    this.add( this.cube4 )

    NC.on('render', this.render)
  }

  setupControls() {

    const controls = new BlenderControls( this.renderer.canvas )
    controls.start( this.renderer.camera )
    // controls.target[2] = 5
window.CAM = this.renderer.camera



    // this.renderer.camera.position = [-4.451544761657715, -27.38461685180664, -125.00590515136719]
    // this.renderer.camera.position = [0,0,35]
    // this.renderer.camera.rotation = [0.8382421135902405, -0.031341902911663055, -0.04845137894153595, -0.5422363877296448]
    // this.renderer.camera.rotation = [-0.0005164391271741398, 0.9992810338915368, 0.014783332813983918, 0.03490890220696463]

    this.renderer.camera.invalidate()

    console.log( this.renderer.camera.position )

    this.controls = controls

  }

  render() {

    this.controls.update()
    this.cube0.render( this.renderer.camera )
    this.cube1.render( this.renderer.camera )
    this.cube2.render( this.renderer.camera )
    this.cube3.render( this.renderer.camera )
    this.cube4.render( this.renderer.camera )

  }

}

module.exports = Scene