'use strict'

const glMatrix = require( 'gl-matrix' )
const vec3 = glMatrix.vec3
const quat = glMatrix.quat
const mat4 = glMatrix.mat4

var V1   = vec3.create(),
    V2   = vec3.create(),
    V3   = vec3.create(),
    IMVP = mat4.create()

var UP = [0, 1, 0]

function setMousePosition(e, el, v) {
  v[0] =  (2 * e.clientX / el.width  - 1)
  v[1] = -(2 * e.clientY / el.height - 1)
}

function FlowerControls($el) {
  this._onMouseMove = this._onMouseMove.bind(this)

  this.$el   = $el
  this.mouse = [0, 0, 1]
}

FlowerControls.prototype = {

  constructor: FlowerControls,

  start: function(camera) {
    this.camera = camera
    this.$el.addEventListener('mousemove', this._onMouseMove)
  },

  stop: function() {
    this.camera = null
    this.$el.removeEventListener('mousemove', this._onMouseMove)
  },

  update: function() {
    // vec3.cross(V1, this.direction, UP) // X
    // vec3.cross(V2, this.direction, V1) // Y

    this.unproject( V1 )
    vec3.scale(V1, V1, 0.01)
    vec3.add( this.camera.position, this.camera.position, V1 )
    this.camera.invalidate()

  },

  _onMouseMove: function(e) {
    setMousePosition(e, this.$el, this.mouse)
  },

  unproject: function( vector ) {
    mat4.invert(IMVP, this.camera.lens._proj)
    vec3.transformMat4( vector, this.mouse, IMVP )
    vec3.scale( vector, vector, Math.abs(30/vector[2]) )
    vec3.transformMat4( vector, vector, this.camera._matrix )
  }

}

module.exports = FlowerControls