import Node from 'nanogl-node'

import {  mat4 } from 'gl-matrix'

var M4 = mat4.create();

class Mesh extends Node {

  constructor( geometry, material ) {

    super()

    this.geometry = geometry
    this.material = material

  }

  render( camera ){

    this.material.prepare( this, camera )

    this.geometry.bind( this.material.prg )
    this.geometry.render()

  }

}

export default Mesh