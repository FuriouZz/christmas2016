import Node from 'nanogl-node'
import PlaneGeometry from 'geometries/plane-geometry'
import {mat4} from 'gl-matrix'

var M4   = mat4.create()

class Plane extends Node {

  constructor( material ){

    super()

    this.material = material
    
    this.geometry = new PlaneGeometry( this.material.gl, {} )    
      
  }

  render( camera ){

    this.material.prepare( this, camera )

    this.geometry.bind( this.material.prg )
    this.geometry.render()

  }

}

export default Plane