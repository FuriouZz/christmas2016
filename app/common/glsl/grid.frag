precision highp float;

void main(){

  float color = 0.3;

  gl_FragColor = vec4( vec3(color), 1.0 );

}