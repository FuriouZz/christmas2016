attribute vec2 aPosition;
attribute vec2 aUv;

uniform mat4 uMVP;

varying vec2 vUv;


void main(){

  gl_Position = uMVP * vec4( aPosition, 0.0, 1.0 );

  vUv = aUv;

}