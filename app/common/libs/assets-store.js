import NetUtils from 'utils/net-utils'

var manifest = []
var assets = {}
var totalAssets = 0
var assetsLoaded = 0

class AssetsStore {

  constructor() {}

  /**
   * Load a manifest
   * According to item type, will choose the adequate loader
   * 
   * @param {Array} manifest
   */
  loadManifest( _manifest ) {

    manifest = _manifest

    for ( let i = 0; i < manifest.length; i++ ) {

      let type = manifest[i].type
      let url  = manifest[i].url
      
      if ( type === 'img' ) this.loadImage( url )
      if ( type === 'json' ) this.loadJson( url )

    }

  }

  /**
   * Called when a single asset is loaded
   * Notify parent the progress
   * Notify parent if complete
   * 
   * @param {String} assetName
   * @param {Image | *} asset
   */
  onAssetLoaded( assetName, asset ){

    // register the asset
    assets[ assetName ] = asset

    // progress
    assetsLoaded++
    var progress = assetsLoaded / manifest.length
    this.onLoadProgress( progress )
    
    // complete /
    if ( progress === 1 ) {
      this.onLoadComplete()
    }

  }

  /**
   * Load an image
   * 
   * @param {String} url
   */
  loadImage( url ){

    let assetNameChunks = url.split('./')
    let assetName = assetNameChunks[ assetNameChunks.length - 1 ]

     NetUtils.loadImage( url ).then( ( asset ) => {
      this.onAssetLoaded( assetName, asset )
    } )

  }

  loadJson( url ) {

    let assetNameChunks = url.split('/')
    let assetName = assetNameChunks[ assetNameChunks.length - 1 ]

    NetUtils.loadJson( url ).then( ( asset ) => {
      this.onAssetLoaded( assetName, JSON.parse( asset ) )
    } )

  }

  /**
   * Return an asset from the cache
   * 
   * @param {String} assetName
   */
  get( assetName ) {

    if ( assets[ assetName ] ) return assets[ assetName ]

    console.error( `[AssetsStore get] asset ${assetName} doesnt exist` )

  }

  /**
   * Callbacks to be overrided by parent
   */
  onLoadProgress( p ) {}
  onLoadComplete() {}

}

var _instance = new AssetsStore

export default _instance