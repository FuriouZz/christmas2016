/**
 * raf.js
 *
 * Global RequestAnimationFrame
 *
 * ----------------------------
 *
 * use example
 *
 * import RAF from 'libs/raf'
 *
 * RAF.subscribe( 'mySubscriberId', mySubscriberFn )
 * RAF.unsubscribe( 'mySubscriberId' )
 * RAF.start()
 * RAF.stop()
 */

const requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})();


const cancelRequestAnimFrame = ( function() {
  return window.cancelAnimationFrame          ||
    window.webkitCancelRequestAnimationFrame  ||
    window.mozCancelRequestAnimationFrame     ||
    window.oCancelRequestAnimationFrame       ||
    window.msCancelRequestAnimationFrame
} )();

let _raf;
let _now = Date.now();
let _lt = _now;
let _elapsedInterval = 0;

class RAF {

  constructor() {

    this.subscribers = [];

    this.update = this._update.bind(this);

    this.dt = 0;
    this.framerate = 16;

    _raf = requestAnimFrame( this.update );

  }

  /**
   * Run all subscribers
   */
  _update() {

    _now = Date.now();

    this.dt = _now - _lt;

    _elapsedInterval += this.dt;

    if ( _elapsedInterval >= this.framerate ) {
      _elapsedInterval = 0;
      this._processUpdate();
    }

    _lt = _now;

    _raf = requestAnimFrame( this.update );

  }

  _processUpdate(){

    for ( let i = 0; i < this.subscribers.length; i++ ) {
      // execute handler
      this.subscribers[i][1]();
    }

  }

  /**
   * Register a new subscriber
   *
   * @param {String} id
   * @param {Function} fn
   */
  subscribe( id, fn ) {

    this.subscribers.push( [ id, fn ] );

  }

  /**
   * Unregister a subscriber
   *
   * @param {String} id
   */
  unsubscribe( id ) {

    for ( let i = 0; i < this.subscribers.length; i++ ) {

      // if id matches, removes
      if ( this.subscribers[i][0] === id ) {
        this.subscribers.splice( i, 1 );
      }

    }

  }

  /**
   * Start globally the RAF
   */
  start() {

    _raf = requestAnimFrame( this.update );

  }

  /**
   * Stop globally the RAF
   */
  stop() {

    cancelRequestAnimFrame( _raf );

  }

  /**
   * start alias
   */
  resume() {

    this.start()

  }

}

const _instance = new RAF();

export default _instance;