function createCanvas( _w, _h ){

  var width           = _w || window.innerWidth
  var height          = _h || window.innerHeight

  var canvas          = document.createElement('canvas')

  canvas.width        = width
  canvas.height       = height
  canvas.style.width  = width + 'px'
  canvas.style.height = height + 'px'

  return canvas

}

function getPixelRatio(){

  return Math.min( Math.max( 1, window.devicePixelRatio ), 2 );

}


export {
  createCanvas,
  getPixelRatio
}