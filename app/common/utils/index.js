import DOMUtils from './dom-utils'
import Ease from './ease'
import GLUtils from './gl-utils'
import NetUtils from './net-utils'
import NumberUtils from './number-utils'

export default {
  DOMUtils,
  Ease,
  GLUtils,
  NetUtils,
  NumberUtils
}