var NetUtils = {

  /**
   * Loads a text file
   *
   * @param {String} url
   * @param {Boolean} fromCache
   * @returns {Promise}
   */
  loadJson: function( url, fromCache = true ) {

    var extension = '';
    if ( !fromCache ) {
      extension = '?' + Math.random()
    }

    url += extension

    var headers = new Headers({ 'Content-Type': 'application/json' })
    var request = new Request( url, { headers: headers } )

    return fetch(request)
      .then( function(response) { return response.text() } )
      .catch( function( error ) { console.error( error ) } )

  },

  /**
   * Loads a text file
   *
   * @param {String} url
   * @param {Boolean} fromCache
   * @returns {Promise}
   */
  loadText: function( url, fromCache ) {

    var extension = ''
    if ( !fromCache ) {
      extension = '?' + Math.random()
    }

    url += extension

    var headers = new Headers({ 'Content-Type': 'text/plain' })
    var request = new Request( url, { headers: headers } )

    return fetch(request)
      .then( function(response) { return response.text() } )
      .catch( function( error ) { console.error( error ) } )

  },

  /**
   * Loads an image
   *
   * @param {String} url
   * @param {Boolean} fromCache
   * @returns {Promise}
   */
  loadImage: function( url ) {

    return new Promise( function( resolve, reject ){

      var image = new Image();

      image.onload = function() {
        resolve( image )
      };
      image.onerror = function() {
        console.error('[NetUtils] coundnt load image', url)
        reject('[NetUtils] coudnt load image')
      };

      image.src = url

    } )

  }

}

export default NetUtils