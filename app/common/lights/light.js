class Light {

  constructor( opts ) {

    this.type = ''

    this.intensity = 1

    this.diffuse = opts.diffuse || [ 1, 1, 1 ]
    this.specular = opts.specular || [ 1, 1, 1 ]

    this._position = new Float32Array(3)

    this.mesh = opts.mesh || null

  }

  get position(){ return this._position }
  set position( newPosition ){
    
    this._position[0] = newPosition[0]
    this._position[1] = newPosition[1]
    this._position[2] = newPosition[2]

    if ( this.mesh ) {
      this.mesh.x = newPosition[0]
      this.mesh.y = newPosition[1]
      this.mesh.z = newPosition[2]
    }
    
  }

}

export default Light