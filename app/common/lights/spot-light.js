import Light from './light'

class SpotLight extends Light {

  constructor(){

    super()

    this.type = 'point'

    this._position = [ 0, 0, 0 ]

  }

}

export default SpotLight