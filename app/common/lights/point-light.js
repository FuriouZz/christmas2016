import Light from './light'

class PointLight extends Light {

  constructor(){

    super()

    this.type = 'point'

    this._position = [ 0, 0, 0 ]

  }

}

export default PointLight