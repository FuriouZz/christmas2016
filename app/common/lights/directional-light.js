import Light from './light'

class DirectionalLight extends Light {

  constructor(){

    super()

    this.type = 'directional'

  }

}

export default DirectionalLight