class AmbientLight {

  constructor( opts ) {

    this.type      = 'ambient'

    this.intensity = opts.intensity || .15
    this.color     = opts.color || [ 1, 1, 1 ]

  }

}

export default AmbientLight