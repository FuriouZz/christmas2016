import Geometry from 'geometries/geometry'

var STRIDE = 2
var BYTES = Float32Array.BYTES_PER_ELEMENT

// X, Y, Z     U, V
var vertices = [
  -.5, -.5, 0.0,   0, 0,
   .5, -.5, 0.0,   1, 0,
   .5,  .5, 0.0,   1, 1,
  -.5,  .5, 0.0,   0, 1
]

var indices = [
  0, 1, 2,
  0, 2, 3
]

class QuadGeometry extends Geometry {

  constructor( gl ) {

    super( gl, vertices, indices )

  }

  allocate(){

    super.allocate()

    var gl = this.gl

    this.attrib( 'aPosition', 3, gl.FLOAT )
    this.attrib( 'aUv', 2, gl.FLOAT )

  }

}

export default QuadGeometry