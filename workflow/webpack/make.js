'use strict'

const ExtractTextPlugin = require("extract-text-webpack-plugin")

const webpack = require('webpack')
const path    = require('path')
const join    = path.join

module.exports = function(o) {

  o = Object.assign({
    watch: false,
    sourcemap: false,
    compress: false
  }, o || {})


  // Stylus loader
  const stylus_params = []
  if (o.sourcemap) stylus_params.push('sourcemap-inline=true')
  if (o.compress)  stylus_params.push('compress=true')
  const stylus_loader = 'stylus?' + stylus_params.join('&')

  // Setup
  const ExtractPlugin = new ExtractTextPlugin("[name]")
  const CWD       = process.cwd()
  const ABSOLUTE_LOAD_PATH = join( CWD, 'app' )
  const ABSOLUTE_DIST_PATH = join( CWD, 'public' )

  const EXPORTS = {

    context: ABSOLUTE_LOAD_PATH,

    watch: o.watch,

    entry: {
      "main.js": "./index.js",
      "vendor.js": "./vendor/index.js",
      "main.css": "styles/index.styl"
    },

    output: {
      path: ABSOLUTE_DIST_PATH,
      filename: "[name]",
      chunkFilename: "[id]"
    },

    resolve: {
      root: ABSOLUTE_LOAD_PATH,
      extensions: [ '', '.js', '.html', '.styl' ],
      alias: {
        camera:     'common/camera',
        entities:   'common/entities',
        geometries: 'common/geometries',
        glsl:       'common/glsl',
        helpers:    'common/helpers',
        libs:       'common/libs',
        lights:     'common/lights',
        materials:  'common/materials',
        utils:      'common/utils'
      }
    },

    module: {
      loaders: [
        {
          test: /\.(js)$/,
          exclude: /(node_modules|bower_components|vendor)/,
          loader: 'babel-loader',
          query: {
            presets: [ 'es2015' ],
            plugins: [ 'transform-runtime' ],
            cacheDirectory: true
          }
        },
        {
          test: /\.styl$/,
          exclude: /(node_modules|bower_components|vendor)/,
          loader: ExtractPlugin.extract(['raw', stylus_loader])
        },
        {
          test: /\.(glsl|frag|vert)$/,
          loader: 'raw',
          exclude: /node_modules/
        },
        {
          test: /\.(glsl|frag|vert)$/,
          loader: 'glslify-loader',
          exclude: /node_modules/
        }
      ]
    },

    stats: {
      colors: true,
      hash: false,
      version: true,
      timings: true,
      assets: false,
      chunks: false,
      chunkModules: false,
      modules: false,
      children: true,
      cached: false,
      reasons: false,
      source: false,
      errorDetails: false,
      chunkOrigins: false
      // context: '',
      // modulesSort: '',
      // chunksSort: '',
      // assetsSort: ''
    },

    cache: !o.compress,
    debug: !o.compress,

    devtool: o.sourcemap ? 'eval' : undefined,

    plugins: [
      ExtractPlugin
    ]

  }

  if (o.compress) {
    EXPORTS.plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        compressor: {
          warnings: false
        }
      })
    )
  }

  return EXPORTS

}